# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  secret_number = rand(1..100)
  num_guesses = 0

  while true
    print "Guess a number between 1 and 100: "
    guess = gets.chomp.to_i
    num_guesses += 1

    accuracy = "too high" if guess > secret_number
    accuracy = "too low" if guess < secret_number
    accuracy = "correct" if guess == secret_number

    print "#{guess} is #{accuracy}! #{num_guesses} guesses."
    break if guess == secret_number
  end
end

def file_shuffler
  print "Please enter a file name: "
  file_name = gets.chomp
  shuffled = File.readlines(file_name).shuffle
  new_file = File.new("#{file_name}-shuffled.txt")

  File.open(new_file, "w") do |file|
    shuffled.each { |line| file.print line }
  end
  
  new_file.close
end
